# Inherit from phone link config
TARGET_PHONE_LINK_SUPPORTED ?= true
ifeq ($(TARGET_PHONE_LINK_SUPPORTED),true)
$(call inherit-product, vendor/microsoft/mms/common/common-vendor.mk)

# Properties
$(call inherit-product, vendor/microsoft/mms/products/properties.mk)

# Customizations
$(call inherit-product, vendor/microsoft/mms/products/custom.mk)
endif
